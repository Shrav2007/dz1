package ru.rsharipov.task11;

import java.io.File;

public class Recurse {
    public static void main(String[] args) {
        File dir = new File("C:\\Users\\nika-\\IdeaProjects\\ru.sharipov\\src\\ru\\rsharipov");
        String spacing = ""; // Отступ
        recursiveOut(dir, spacing);
    }

        private static void recursiveOut(File dir, String spacing) {
            File[] files = dir.listFiles(); // Получаем массив файлов из директории
            System.out.println(spacing + dir.getName()); // Выводим имя директории
            spacing = spacing.concat(" "); // Добавляем пробел к отступу

            for (File file : files) {
                if (file.isFile()) { // Это файл?
                    System.out.println(spacing + file.getName()); // Выводим имя файла
                } else {
                    recursiveOut(file, spacing);
                }
            }
        }
}
