package ru.rsharipov.task11;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

public class WorkWithFile {
    public static void main(String[] args) {

        File file = new File("work.txt"); // Создание файла work.txt
        try {
            file.createNewFile();
            System.out.println("Файл " + file.getName() + " создан");
        } catch (IOException e) {
            System.out.println("Не удалось создать файл: " + e.getMessage());
        }

        File newFile = new File("rework.txt"); // Переименование файла в rework.txt
        file.renameTo(newFile);
        System.out.println("Файл переименован");

        try {
            Files.copy(newFile.toPath(), Paths.get("C:\\Users\\nika-\\copy.txt"), // Копирование файла rework.txt
                    StandardCopyOption.REPLACE_EXISTING);
            System.out.println("Файл скопирован");
        } catch (IOException e) {
            e.printStackTrace();
        }

        newFile.delete(); // Удаление файлa rework.txt
    }
}
