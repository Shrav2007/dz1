package ru.rsharipov.task5;

import java.util.Scanner;

import static ru.rsharipov.task5.App.logger;

public class VendingMachine {
    private Drink drinks[];

    public VendingMachine(Drink[] drinks) {
        this.drinks = drinks;
    }

    public void showMenu () { // Вывод меню

        logger.info("Вызван метод showMenu");
        System.out.println("МЕНЮ:");
        System.out.println("1 - Чай (20 руб.)");
        System.out.println("2 - Кофе (30 руб.)");
        System.out.println("3 - Какао (25 руб.)");
        System.out.println("4 - Шоколад (28 руб.)");
        System.out.println("5 - Кола (35 руб.)");
        System.out.println("6 - Фанта (35 руб.)");
        System.out.println("7 - Спрайт (35 руб.)\n");
    }

    public int addMoney (int money) { // Зачисление денег на счет
        logger.info("Вызван метод addMoney с параметром <" + money + ">");
        System.out.print("Вставьте деньги в автомат: ");
        Scanner scan = new Scanner(System.in);
        money = scan.nextInt();
        return money;
    }

    public void giveMeADrink (int key, int money) { // Выдача напитка
        logger.info("Вызван метод giveMeADrink с параметрами <" + key + "> и <" + money + ">");
        if (money == 0) { // Пользователь не внес деньги
            logger.warn("Пользователь не внес деньги");
            System.out.println("Вы не внесли деньги!");
        }
        else {
            System.out.print("Выберите напиток: ");
            Scanner scan = new Scanner(System.in);
            key = scan.nextInt();
            if (key > drinks.length) { // Пользователь выбрал несуществующий номер напитка
                logger.warn("Пользователь выбрал несуществующий номер напитка");
                System.out.println("Нет такого напитка!");
            }
            else if (money < drinks[key-1].getPrice()) { // Пользователь выбрал более дорогой напиток
                logger.warn("Пользователь выбрал более дорогой напиток");
                System.out.println("Недостаточно денег на счете!");
            }
            else
                System.out.println("Возьмите свой напиток - " + drinks[key-1].getTitle());
        }
    }
}
