package ru.rsharipov.task5;

public enum Drink {
    TEA("Чай", 20), COFFEE("Кофе", 30), CACAO( "Какао", 25),
    CHOCOLATE( "Шоколад", 28), COKE( "Кола", 35), FANTA( "Фанта", 35),
    SPRITE( "Спрайт", 35);

    Drink(String title, int price) {

        this.title = title;
        this.price = price;
    }

    private String title; // Название напитка
    private int price; // Цена напитка

    public String getTitle() {
        return title;
    }

    public int getPrice() {
        return price;
    }
}
