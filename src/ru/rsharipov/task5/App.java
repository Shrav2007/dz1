package ru.rsharipov.task5;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

public class App {
    public static final Logger logger = Logger.getLogger(App.class);

    public static void main(String[] args) {

        logger.log(Priority.INFO, "Начало работы программы");
        Drink[] drinks = Drink.values(); // Получение массива напитков из enum
        VendingMachine vm = new VendingMachine(drinks);
        vm.showMenu();
        int money = vm.addMoney(0);
        vm.giveMeADrink(0, money);
        logger.info("Программа завершена");

    }
}
