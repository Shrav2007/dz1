package ru.rsharipov.task8;

import java.util.Scanner;

public class Censor {
    public static void main(String[] args) {
        System.out.print("Введите строку: ");
        Scanner scan = new Scanner(System.in);
        String str = scan.nextLine();
        String str2 = str.replaceAll("(?iu)бяка","<вырезано цензурой>");
        System.out.println("Ваша строка после применения цензуры: " + str2);
    }
}
