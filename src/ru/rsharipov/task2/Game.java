package ru.rsharipov.task2;

import java.util.Scanner;

public class Game {

    public static void main(String[] args) {

        int randomNumber = (int) (Math.random() * 100); // Генерация случайного числа от 0 до 100
        int prevNumber = 0; // Для первого ввода пользователя считаем предыдущим введенным числом 0
        System.out.println("Угадайте число от 0 до 100 (для выхода из игры введите любую букву):");
        Scanner in = new Scanner(System.in);

        if (in.hasNextInt()) { // Проверяем введено ли целое число
            int nextNumber = in.nextInt();

            while (nextNumber != randomNumber) { // Проверяем угадано ли число
                if ((Math.abs(randomNumber - nextNumber) <= Math.abs(randomNumber - prevNumber)))
                    System.out.println("Горячо!");
                else System.out.println("Холодно!");

                System.out.println("Попробуйте еще: ");
                prevNumber = nextNumber;
                in = new Scanner(System.in);

                if (in.hasNextInt())
                    nextNumber = in.nextInt();
                else System.exit(0); // Выход из игры до отгадывания числа
            }
            System.out.println("Угадали!");
        }
     }
}
