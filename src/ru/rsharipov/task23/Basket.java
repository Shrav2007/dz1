package ru.rsharipov.task23;

import java.util.List;

public interface Basket {
    void addProduct(String product, int quantity); // Добавить товар с соответствующим количеством в корзину
    void removeProduct(String product); // Удалить товар из корзины по наименованию
    void updateProductQuantity(String product, int quantity); // Изменить количество соответствующего товара
    void clear(); // Очистить корзину
    List<String> getProducts(); // Получить содержимое корзины
    int getProductQuantity(String product); // Получить количество товара по наименованию
}
