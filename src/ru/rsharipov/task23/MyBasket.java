package ru.rsharipov.task23;

import java.util.Iterator;
import java.util.List;

public class MyBasket implements Basket{

    List<Product> myBasket;

    public MyBasket(List<Product> myBasket) {
        this.myBasket = myBasket;
    }

    public List<Product> getMyBasket() {
        return myBasket;
    }

    public void setMyBasket(List<Product> myBasket) {
        this.myBasket = myBasket;
    }

    @Override
    public void addProduct(String product, int quantity) {
        myBasket.add(new Product(product, quantity));
    }

    @Override
    public void removeProduct(String product) {

        Iterator<Product> it = myBasket.iterator();
        while (it.hasNext()) {
            Product products = it.next();
            if (products.getProduct().equals(product)) {
                it.remove();
            }
        }
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        for (Product products : myBasket) {
            if (products.getProduct().equals(product)) {
                products.setQuantity(quantity);
            }
        }
    }

    @Override
    public void clear() {
        myBasket.clear();
    }

    @Override
    public List<String> getProducts() {
        System.out.println(getMyBasket());
        return null;
    }

    @Override
    public int getProductQuantity(String product) {
        for (Product products : myBasket) {
            if (products.getProduct().equals(product)) {
                return products.getQuantity();
            }
        }
        return 0;
    }
}
