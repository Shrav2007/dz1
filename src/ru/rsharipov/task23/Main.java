package ru.rsharipov.task23;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Product> myList = new ArrayList<>();
        MyBasket mb = new MyBasket(myList);

        mb.addProduct("Футболка", 2);
        mb.addProduct("Брюки", 1);
        mb.addProduct("Носки", 5);
        mb.addProduct("Рубашка", 3);
        mb.addProduct("Жакет", 1);
        mb.getProducts();

        System.out.println(mb.getProductQuantity("Рубашка"));
        mb.updateProductQuantity("Рубашка", 2);
        mb.getProducts();

        mb.removeProduct("Жакет");
        mb.getProducts();

        mb.clear();
        mb.getProducts();
    }
}
