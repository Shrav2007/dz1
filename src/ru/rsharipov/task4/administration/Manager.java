package ru.rsharipov.task4.administration;

import ru.rsharipov.task4.People;

import java.time.LocalDate;

public class Manager extends People {
    public Manager(String name, String surname, LocalDate birthday) {
        super(name, surname, birthday);
    }
}

