package ru.rsharipov.task4.administration;

import ru.rsharipov.task4.People;

import java.time.LocalDate;

public class Coach extends People {

    public Coach(String name, String surname, LocalDate birthday, int salary) {
        super(name, surname, birthday);
        this.salary = salary;
    }

    private int salary;

    int getSalary() {
        return salary;
    }

    void setSalary(int salary) {
        this.salary = salary;
    }
}
