package ru.rsharipov.task4;

import java.time.LocalDate;
public class FootballSection {

    public static void main(String[] args) {

        Player amir = new Player("Амир", "Шарипов", LocalDate.of(2015, 2, 13));
        Player karim = new Player("Карим", "Гашин", LocalDate.of(2015, 8, 26));
        amir.setAgeGroup("3-5");
        karim.setAgeGroup("3-5");
        Group g1 = new Group();
        g1.setTitle("Начинающая");
        g1.setPlayers(new Player[]{amir, karim});

        System.out.println(g1);
    }
}
