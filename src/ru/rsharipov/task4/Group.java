package ru.rsharipov.task4;

import java.util.Arrays;

public class Group {
    private String title;
    private Player[] players = new Player[12];

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Player[] getPlayers() {
        return players;
    }

    public void setPlayers(Player[] players) {
        this.players = players;
    }

    @Override
    public String toString() {
        return "Group{" +
                "title='" + title + '\'' +
                ", players=" + Arrays.toString(players) +
                '}';
    }
}
