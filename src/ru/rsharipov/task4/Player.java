package ru.rsharipov.task4;

import java.time.LocalDate;

public class Player extends People {

    private String ageGroup;

    public Player(String name, String surname, LocalDate birthday) {
        super(name, surname, birthday);
    }

    public String getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
    }

    @Override
    public String toString() {
        return "Player{" +
                "ageGroup='" + ageGroup + '\'' +
                ", name='" + getName() + '\'' +
                ", surname='" + getSurname() + '\'' +
                ", birthday=" + getBirthday() +
                '}';
    }
}
