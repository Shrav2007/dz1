package ru.rsharipov.task29;

import java.util.Objects;

public class Person {
    private int age;
    private String lastName;
    private String sex;

    public Person(int age, String lastName, String sex) {
        this.age = age;
        this.lastName = lastName;
        this.sex = sex;
    }

/*    public Person() {
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (age != person.age) return false;
        if (!lastName.equals(person.lastName)) return false;
        return sex.equals(person.sex);
    }

    @Override
    public int hashCode() {
        int result = lastName.hashCode();
        result = 31 * result + sex.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", lastName='" + lastName + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }
}
