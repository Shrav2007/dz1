package ru.rsharipov.task1;

import java.util.Scanner; // импортируем класс для ввода с консоли

public class Gas {

    public static void main(String[] args) {

        double price = 42.5; // задаём цену за 1 л бензина
        Scanner in = new Scanner(System.in);
        System.out.print("Введите количество литров бензина: ");
        double amount = in.nextFloat();
        double cost = amount * price; // считаем общую стоимость
        System.out.printf("Общая стоимость бензина %.2f руб.\n", cost);
        in.close();
    }
}
