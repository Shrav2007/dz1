package ru.rsharipov.task1;

import java.util.Scanner; // импортируем класс для ввода с консоли

public class Time {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Введите количество секунд: ");
        double second = in.nextFloat();
        double hour = second / 3600; // переводим секунды в часы
        System.out.printf("%.0f сек = %f ч.\n", second, hour);
        in.close();
    }
}

