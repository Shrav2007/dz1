package ru.rsharipov.task1;

import java.util.Scanner; // импортируем класс для ввода с консоли

public class Salary {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Введите зарплату: ");
        double totalSalary = in.nextFloat();
        double paycheck = totalSalary * 0.87; // считаем зарплату за вычетом НДФЛ (13%)
        System.out.printf("Зарплата за вычетом НДФЛ составляет: %.2f руб.\n", paycheck);
        in.close();
    }
}
