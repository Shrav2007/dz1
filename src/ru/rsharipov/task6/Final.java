package ru.rsharipov.task6;

import java.util.Scanner;

public class Final {
    final static double PI = 3.14; // Неизменяемое поле (число пи)

    public static void main(String[] args) {
        System.out.print("Введите диаметр окружности в см: ");
        Scanner in = new Scanner(System.in);
        int diameter = in.nextInt();
        System.out.printf("Длина окружности равна %.2f см", PI*diameter);
    }
}
