package ru.rsharipov.task6;

public class Count {
    static int counter = 0; // Создаем счетчик созданных объектов
    private String title;
    private int calorific;

    public Count(String title, int calorific) {
        this.title = title;
        this.calorific = calorific;
        counter++; // Увеличиваем счетчик
    }
}