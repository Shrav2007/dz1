package ru.rsharipov.task6;

import java.time.LocalDate;

public class Converter {
    public static void main(String[] args) {
        Dogovor dog = new Dogovor(157, LocalDate.of(2019, 02,15),
                new String[]{"Куртка", "Костюм", "Брюки"}); // Заполняем договор
        Act aux = convert(dog); // Вызываем метод конвертации
        System.out.println(aux); // Печатаем акт
    }

    public static Act convert(Dogovor dog){
        Act aux = new Act(dog.getNumberDog(), dog.getDateDog(), dog.getGoodsDog()); // Конвертируем договор в акт
        return aux;
    }
}
