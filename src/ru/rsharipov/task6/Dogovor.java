package ru.rsharipov.task6;

import java.time.LocalDate;
import java.util.Arrays;

public class Dogovor {
    private int numberDog;
    private LocalDate dateDog;
    private String[] goodsDog;

    public Dogovor(int numberDog, LocalDate dateDog, String[] goodsDog) {
        this.numberDog = numberDog;
        this.dateDog = dateDog;
        this.goodsDog = goodsDog;
    }

    public int getNumberDog() {
        return numberDog;
    }

    public void setNumberDog(int numberDog) {
        this.numberDog = numberDog;
    }

    public LocalDate getDateDog() {
        return dateDog;
    }

    public void setDateDog(LocalDate dateDog) {
        this.dateDog = dateDog;
    }

    public String[] getGoodsDog() {
        return goodsDog;
    }

    public void setGoodsDog(String[] goodsDog) {
        this.goodsDog = goodsDog;
    }

    @Override
    public String toString() {
        return "Dogovor{" +
                "numberDog=" + numberDog +
                ", dateDog=" + dateDog +
                ", goodsDog=" + Arrays.toString(goodsDog) +
                '}';
    }
}
