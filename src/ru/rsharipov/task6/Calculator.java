package ru.rsharipov.task6;

import java.util.Scanner;

public class Calculator {
    public static void Calc(int operation, double firstVar, double secVar) { // Статический метод
        switch (operation) {
            case 1 :
                System.out.println("Сумма равна " + (firstVar + secVar));
                break;
            case 2 :
                System.out.println("Разность равна " + (firstVar - secVar));
                break;
            case 3 :
                System.out.println("Произведение равно " + (firstVar * secVar));
                break;
            case 4 :
                System.out.println("Деление равно " + (firstVar / secVar));
                break;
            case 5 :
                System.out.printf("%.2f процентов от %.2f равно %.2f", firstVar, secVar, (firstVar*secVar/100));
                break;
            default :
                System.out.println("Неверные данные!");
        }
    }

    public static void main(String[] args) {

        System.out.println("1 - Сложение (a+b);"); // Вывод "калькулятора" в консоль
        System.out.println("2 - Вычитание (a-b);");
        System.out.println("3 - Умножение (a*b);");
        System.out.println("4 - Деление (a/b);");
        System.out.println("5 - Взятие процента (a% от b).");
        System.out.print("Выберите тип операции (нажав 1, 2, 3, 4 или 5): ");
        Scanner scan = new Scanner(System.in);
        int operation = scan.nextInt();

        System.out.print("Введите первое число a: ");
        scan = new Scanner(System.in);
        double firstVar = scan.nextDouble();

        System.out.print("Введите второе число b: ");
        scan = new Scanner(System.in);
        double secVar = scan.nextDouble();
        Calc(operation, firstVar, secVar); // Вызов метода Calc
    }
}
