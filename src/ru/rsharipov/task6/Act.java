package ru.rsharipov.task6;

import java.time.LocalDate;
import java.util.Arrays;

public class Act {
    private int numberAct;
    private LocalDate dateAct;
    private String[] goodsAct;

    public Act(int numberAct, LocalDate dateAct, String[] goodsAct) {
        this.numberAct = numberAct;
        this.dateAct = dateAct;
        this.goodsAct = goodsAct;
    }

    public int getNumberAct() {
        return numberAct;
    }

    public void setNumberAct(int numberAct) {
        this.numberAct = numberAct;
    }

    public LocalDate getDateAct() {
        return dateAct;
    }

    public void setDateAct(LocalDate dateAct) {
        this.dateAct = dateAct;
    }

    public String[] getGoodsAct() {
        return goodsAct;
    }

    public void setGoodsAct(String[] goodsAct) {
        this.goodsAct = goodsAct;
    }

    @Override
    public String toString() {
        return "Act{" +
                "numberAct=" + numberAct +
                ", dateAct=" + dateAct +
                ", goodsAct=" + Arrays.toString(goodsAct) +
                '}';
    }
}
