package ru.rsharipov.task6;

public class Target {
    public static void main(String[] args) {

        Count obj1 = new Count("Яблоко", 45);
        Count obj2 = new Count("Груша", 42);
        Count obj3 = new Count("Банан", 89);
        Count obj4 = new Count("Киви", 67);
        Count obj5 = new Count("Арбуз", 38);
        System.out.println("Объектов типа Count создано: " + Count.counter);
    }
}
