package ru.rsharipov.task14;

import java.io.*;
import java.text.*;
import java.util.Scanner;

public class CashVoucher {
    public static void main(String[] args) {
        try {
            FileInputStream fis = new FileInputStream("products.txt"); // Входной файл с покупками
            Scanner scanner = new Scanner(fis);

            // Выходной файл с кассовым чеком
            PrintStream os = new PrintStream(new File ("C:/Users/nika-/IdeaProjects/ru.sharipov/out.txt"));

            float total = 0; // Поле "итого"

            os.printf("%-17s%7s%9s%14s%n", "Наименование", "Цена", "Кол-во", "Стоимость"); // Вывод шапки чека
            os.println("===============================================");
            while (scanner.hasNext()) { // Проверка конца файла
                String name = scanner.nextLine(); // Поле "наименование"
                float quantity = Float.parseFloat(scanner.nextLine()); // Поле "количество"
                float price = Float.parseFloat(scanner.nextLine()); // Поле "цена"
                DecimalFormat dfSum = new DecimalFormat("=###.00"); // Формат вывода "итого"
                DecimalFormat dfQuan = new DecimalFormat("###.###"); // Формат вывода "количество"
                float sum = quantity * price;
                os.printf("%-17s%7.2f x %6s%14s%n", name, price, dfQuan.format(quantity), dfSum.format(sum)); // Вывод
                // строк чека
                total += quantity * price;
            }
            os.println("===============================================");
            os.printf("Итого:%41.2f", total); // Вывод "итого"
            System.out.println("Чек создан!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
