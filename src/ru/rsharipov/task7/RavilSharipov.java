package ru.rsharipov.task7;

public class RavilSharipov extends Man implements ManRun, ManSwim {
    @Override
    public void Running() {
        System.out.println("Я бегаю по утрам");
    }

    @Override
    public void Brass() {
        System.out.println("Я не умею плавать брассом");
    }

    @Override
    public void Сrawl() {
        System.out.println("Я умею и люблю плавать кролем");
    }
}
