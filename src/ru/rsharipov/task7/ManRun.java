package ru.rsharipov.task7;

public interface ManRun {
    default void Running() {
        System.out.println("Я бегаю");
    }

    default void Speed() {
        System.out.println("Я могу бежать со скоростью 10 км/ч");
    }

}
