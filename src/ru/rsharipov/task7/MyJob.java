package ru.rsharipov.task7;

public class MyJob {
    public static void main(String[] args) {
        Animal cat = new Cat();
        cat.getName();
        Animal dog = new Dog();
        dog.getName();
        Animal mouse = new Mouse();
        mouse.getName();
        Animal rooster = new Rooster();
        rooster.getName();
    }
}
