package ru.rsharipov.task7;

public class UsainBolt extends Man implements ManRun, ManSwim {
    @Override
    public void Running() {
        System.out.println("Я бегаю и зарабатываю на этом деньги");
    }

    @Override
    public void Speed() {
        System.out.println("Я могу бежать со скоростью 44 км/ч");
    }
}
