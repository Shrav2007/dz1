package ru.rsharipov.task17;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App {
    public static void main(String[] args) {
        List<Person> people = new ArrayList<>();
        people.add(new Person("Вероника", 30));
        people.add(new Person("Равиль", 31));
        people.add(new Person("Амир", 4));
        people.add(new Person("Амир", 2));
        people.add(new Person("Равиль", 55));
        people.add(new Person("Вероника", 65));
        Collections.sort(people, new PersonSuperComparator());
        for (Person p : people)
            System.out.println (p.toString());
    }
}
