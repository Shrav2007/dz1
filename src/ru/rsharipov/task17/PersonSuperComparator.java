package ru.rsharipov.task17;

import java.util.Comparator;

public class PersonSuperComparator implements Comparator<Person> {
    public int compare(Person a, Person b) {
        int result = a.getName().compareTo(b.getName()); // Сравниваем по имени
        if (result != 0) {
            return result;
        }
        result = a.getAge() - b.getAge(); // Если имена равны, сравниваем по возрасту
        return result;
    }
}
