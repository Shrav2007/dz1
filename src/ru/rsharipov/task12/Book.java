package ru.rsharipov.task12;

import java.io.Serializable;

public class Book implements Serializable {
    private static final long serialVersionUID = 555L;

    private String name;
    private String author;
    private int PublicationYear;

    public Book(String name, String author, int publicationYear) {
        this.name = name;
        this.author = author;
        PublicationYear = publicationYear;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPublicationYear() {
        return PublicationYear;
    }

    public void setPublicationYear(int publicationYear) {
        PublicationYear = publicationYear;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", PublicationYear=" + PublicationYear +
                '}';
    }
}
