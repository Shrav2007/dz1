package ru.rsharipov.task12;

import java.util.Scanner;

import static ru.rsharipov.task12.Library.loadLibrary;

public class App {

    public static void main(String[] args) throws Exception {

        Library library = loadLibrary();

        library.listBooks();

        System.out.print("Для добавления книги нажмите 1, для выхода любую другую цифру: ");
        Scanner scan = new Scanner(System.in);
        int key = scan.nextInt();
        if (key == 1) {
            library.addBook();
        }

        library.saveLibrary(library);
    }
}
