package ru.rsharipov.task12;

import java.io.*;
import java.util.Scanner;

public class Library implements Serializable {
    private static final long serialVersionUID = -1212344216472967054L;

    private static final String fileName = "Books.bin"; // Задаем имя файла библиотеки
    static boolean empty = false; // Признак непустой библиотеки
    private Book[] books;

    public Book[] getBooks() {
        return books;
    }

    public void setBooks(Book[] books) {
        this.books = books;
    }

    /**
     * Добавление книги в библиотеку
     */
    public void addBook () {

        int leng;

        if (empty) { // Если библиотека пустая, создается массив из одного элемента
            empty = false;
            leng = 1;
        } else { // Если библиотека непустая, создаем массив из [books.length+1] элементов
            leng = books.length + 1;
        }

        Book[] newBooks = new Book[leng];

        if (leng != 1) {
            System.arraycopy(books, 0, newBooks, 0, books.length); // Копирование массива
        }

        newBooks[leng-1] = new Book("-","-", 0); // Инициализируем элемент в массиве
        // перед доступом к нему

        System.out.println("Введите название книги: ");
        Scanner in = new Scanner(System.in);
        String name = in.nextLine();
        newBooks[leng-1].setName(name);

        System.out.println("Введите автора книги: ");
        in = new Scanner(System.in);
        String author = in.nextLine();
        newBooks[leng-1].setAuthor(author);

        System.out.println("Введите год издания книги: ");
        in = new Scanner(System.in);
        int publicationYear = in.nextInt();
        newBooks[leng-1].setPublicationYear(publicationYear);

        books = newBooks;
    }

    /**
     * Вывод книг из библиотеки на консоль
     */
    public void listBooks (){
        if (empty) {
            System.out.println("Библиотека пустая!");
        } else {
            for (Book book : books) {
                System.out.println(book);
            }
        }
    }

    /**
     * Чтение библиотеки из файла
     * Если файл не существует, он создается
     * @return Библиотека
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Library loadLibrary() throws IOException, ClassNotFoundException {

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
            return (Library) ois.readObject();
        } catch (FileNotFoundException e) {
            File file = new File(fileName);
            file.createNewFile();
            empty = true; // Признак пустой библиотеки
            return new Library();
        }
    }

    /**
     * Сохранение библиотеки в файл
     * @param library Библиотека
     */
    public void saveLibrary(Library library) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
            oos.writeObject(library);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
