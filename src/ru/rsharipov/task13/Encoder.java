package ru.rsharipov.task13;

import java.io.*;
import java.nio.charset.Charset;

public class Encoder {
    public static void main(String[] args) throws IOException {
        File file1 = new File("Windows-1251.txt");
        File file2 = new File("KOI8.txt");

        try (OutputStream os = new FileOutputStream(file1)){
            String s = "Это моя работа";
            os.write(s.getBytes("Windows-1251"));
        }

        try (InputStream is = new FileInputStream("Windows-1251.txt")) {
            byte[] buf = new byte[100];
            if(is.read(buf) != -1) {
                String s = new String(buf, "KOI8");

                try (OutputStream os = new FileOutputStream(file2)){
                    Charset koi8 = Charset.forName("KOI8");
                    os.write(s.getBytes(koi8));
                }
            }
        }
    }
}
