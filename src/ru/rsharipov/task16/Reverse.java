package ru.rsharipov.task16;

public class Reverse {
    public static void main(String[] args) {
        int[] myArr = new int[]{13, 17, 2, 5, 7, 88, 87, 95};
        System.out.println("Массив до преобразования: ");
        toPrint (myArr);
        toReverce(myArr);
        System.out.println("Массив после преобразования: ");
        toPrint (myArr);
    }

    /**
     * Реверс массива
     * @param myArr Массив
     */
    private static void toReverce(int[] myArr) {
        for (int i = 0; i < (myArr.length / 2); i++) {
            int x = myArr[i]; // Вспомогательная переменная
            myArr[i] = myArr[myArr.length - (i+1)];
            myArr[myArr.length - (i+1)] = x;
        }
    }

    /**
     * Вывод массива в консоль
     * @param myArr Массив
     */
    private static void toPrint(int[] myArr) {
        for (int i = 0; i < myArr.length; i++) {
            System.out.print(myArr[i] + " ");
        }
        System.out.println();
    }
}
