package ru.rsharipov.task16;

public class Shift {
    public static void main(String[] args) {
        int[][] myArray = new int[10][10]; // Создание двумерного массива 10х10 типа int
        for (int i = 0; i < myArray.length; i++) { // Заполнение массива данными
            for (int j = 0; j < myArray[i].length; j++) {
                myArray[i][j] = i + j;
            }
        }

        System.out.println("Массив до преобразования: ");
        toPrint (myArray);
        toLeft(myArray);
        System.out.println("Массив после преобразования: ");
        toPrint (myArray);
    }

    /**
     * Вывод массива в консоль
     * @param myArray Массив
     */
    private static void toPrint(int[][] myArray) {
        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                System.out.printf("%2d ", myArray[i][j]);
            }
            System.out.println();
        }
    }

    /**
     * Сдвиг элементов массива влево
     * @param myArray Массив
     */
    private static void toLeft(int[][] myArray) {
        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                if ((i == (myArray[j].length - 1)) && j == (myArray[i].length - 1)) {
                    myArray[i][j] = 0; // Последнему элементу присвоить 0
                } else if (j == (myArray[i].length - 1)) { // В текущую ячейку поместить значение следующей
                    myArray[i][j] = myArray[i+1][0];
                }
                else {
                    myArray[i][j] = myArray[i][j + 1];
                }
            }
        }
    }
}
