package ru.rsharipov.task9;

public enum Food {
    APPLE (1,"яблоко", "yes"), CARROT (2,"морковь", "no"),
    CUCUMBER (3,"огурец", "no"), KIWI (4,"киви", "yes"),
    WATERMELON (5,"арбуз","yes"), TOMATO (6,"помидор","yes");

    Food(int key, String title, String tasty) {
        this.key = key;
        this.title = title;
        this.tasty = tasty;
    }

    private int key; // Кнопка выбора еды
    private String title; // Название еды
    private String tasty; // Вкусно или невкусно?

    public int getKey() {
        return key;
    }

    public String getTitle() {
        return title;
    }

    public String getTasty() {
        return tasty;
    }
}
