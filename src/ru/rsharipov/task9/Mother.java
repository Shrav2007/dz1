package ru.rsharipov.task9;

public class Mother {
    public static void main(String[] args) {
        Food[] foods = Food.values(); // Получение массива еды из enum
        Child child = new Child();
        try {
            child.doEat(foods);
        } catch (FoodException e) {
            System.out.println("Тьфу!"); // Ребенок выплевывает невкусную еду
        }
        finally {
            System.out.println("Спасибо большое, мама!"); // Ребенок всегда благодарит маму
        }
    }
}
