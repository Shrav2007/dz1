package ru.rsharipov.task9;

import java.util.Scanner;

public class Child {
    public void doEat(Food[] foods) throws FoodException {
        for (Food s : Food.values()) // Вывод еды
            System.out.println(s.getKey() + ": " + s.getTitle());
        System.out.print("Выберите еду, нажав соответствующую цифру: ");
        Scanner scan = new Scanner(System.in);
        int key = scan.nextInt();
        if (foods[key - 1].getTasty().equals("no")) { // Проверка еды на вкус
            throw new FoodException(); // Еда невкусная
            }
        else {
            System.out.println("Съел " + foods[key - 1].getTitle() + " за обе щеки"); // Еда вкуснвя
            }
        }
}
class FoodException extends Exception {};

