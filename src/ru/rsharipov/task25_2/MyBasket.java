package ru.rsharipov.task25_2;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MyBasket implements Basket {

    private Map<String, Integer> myBasket = new HashMap<String, Integer>();

    public MyBasket(Map<String, Integer> myBasket) {
        this.myBasket = myBasket;
    }

    public Map<String, Integer> getMyBasket() {
        return myBasket;
    }

    public void setMyBasket(Map<String, Integer> myBasket) {
        this.myBasket = myBasket;
    }

    @Override
    public void addProduct(String product, int quantity) {
        myBasket.put(product, quantity);
    }

    @Override
    public void removeProduct(String product) {

        Iterator<Map.Entry<String, Integer>> it = myBasket.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> products = it.next();
            if (products.getKey().equals(product)) {
                it.remove();
            }
        }
    }

    @Override
    public void updateProductQuantity(String product, int quantity) {
        for (Map.Entry<String, Integer> entry : myBasket.entrySet()) {
            if (entry.getKey().equals(product)) {
                entry.setValue(quantity);
            }
        }
    }

    @Override
    public void clear() {
        myBasket.clear();
    }

    @Override
    public List<String> getProducts() {
        System.out.println(getMyBasket());
        return null;
    }

    @Override
    public int getProductQuantity(String product) {
        for (Map.Entry<String, Integer> entry : myBasket.entrySet()) {
            if (entry.getKey().equals(product)) {
                return entry.getValue();
            }
        }
        return 0;
    }
}
