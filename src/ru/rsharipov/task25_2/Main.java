package ru.rsharipov.task25_2;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        Map<String, Integer> map = new HashMap<String, Integer>();
        MyBasket mb = new MyBasket(map);

        mb.addProduct("Молоко", 2);
        mb.addProduct("Хлеб", 1);
        mb.addProduct("Чай", 1);
        mb.addProduct("Печенье", 2);
        mb.addProduct("Шоколад", 3);
        mb.getProducts();

        System.out.println(mb.getProductQuantity("Хлеб"));
        mb.updateProductQuantity("Хлеб", 3);
        mb.getProducts();

        mb.removeProduct("Чай");
        mb.getProducts();

        mb.clear();
        mb.getProducts();
    }
}
