package ru.rsharipov.task15;

import com.fasterxml.jackson.annotation.*;

public class Name {
    private String name;
    private String surname;
    private String sex;
    private String region;

    public Name(String name, String surname, String sex, String region) {
        this.name = name;
        this.surname = surname;
        this.sex = sex;
        this.region = region;
    }

    public Name() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @JsonProperty(value = "gender")
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public String toString() {
        return "Ваши случайные данные {" +
                "имя = '" + name + '\'' +
                ", фамилия = '" + surname + '\'' +
                ", пол = '" + sex + '\'' +
                ", регион = '" + region + '\'' +
                '}';
    }
}
