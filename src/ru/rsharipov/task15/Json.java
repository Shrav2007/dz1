package ru.rsharipov.task15;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.URL;

public class Json {
    public static void main(String[] args) {
        try {
            URL url = new URL("https://uinames.com/api/");
            try (InputStream is = url.openStream();
                 Reader reader = new InputStreamReader(is);
                 BufferedReader br = new BufferedReader(reader);
            ){
                ObjectMapper objectMapper = new ObjectMapper();
                String s = br.readLine();
                Name name = objectMapper.readValue(s, Name.class);
                System.out.println(name);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
