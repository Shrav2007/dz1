package ru.rsharipov.task25_1;

import java.util.HashMap;
import java.util.Map;

public class App {
    public static void main(String[] args) {
        MapFilter mf = new MapFilter(null);
        Map<String, String> map = new HashMap<String, String>();
        map.put("Вася", "Иванов");
        map.put("Петр", "Петров");
        map.put("Виктор", "Сидоров");
        map.put("Сергей", "Савельев");
        map.put("Вадим", "Викторов");
        System.out.println(map);
        System.out.println(mf.isUnique (map));
    }
}
