package ru.rsharipov.task25_1;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

public class MapFilter {
    Map<String, String> map;

    public MapFilter(Map<String, String> map) {
        this.map = map;
    }

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }

    @Override
    public String toString() {
        return "MapFilter{" +
                "map=" + map +
                '}';
    }

    public boolean isUnique(Map<String, String> map) {
        int repeat = 0;
        Collection<String> values = map.values(); // Получаем множество values из map
        Iterator<String> iter1 = values.iterator();
        Iterator<String> iter2 = values.iterator();

        while (iter1.hasNext()) {
            String temp = iter1.next();
            while (iter2.hasNext()) {
                if (iter2.next().equals(temp)) { // Сравниваем values
                    repeat++;
                    if (repeat > 1) {
                        return false; // В map есть два и более одинаковых value
                    }
                }
            }
            iter2 = values.iterator();
            repeat = 0;
        }
        return true; // В map нет двух и более одинаковых value
    }
}
