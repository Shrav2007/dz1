package ru.rsharipov.task3;

import java.util.Scanner;

public class DescribeNum {

    public static void main(String[] args) {
        System.out.print("Введите число: ");
        Scanner input = new Scanner(System.in);
        int numeric = input.nextInt();

        if (numeric < 0) // Проверяем число на отрицательность
            System.out.print("Число отрицательное, " );
        else if (numeric == 0) // Проверяем равно ли число нулю
            System.out.print("Число нулевое, " );
        else System.out.print("Число положительное, ");

        if (numeric % 2 == 0) // Проверяем число на четность
            System.out.println("четное." );
        else System.out.println("нечетное.");
    }
}
