package ru.rsharipov.task3;

import java.util.Scanner;

public class Minimum {

    public static void main(String[] args) {

        System.out.print("Введите первое число: ");
        Scanner scan = new Scanner(System.in);
        int firstNum = scan.nextInt(); // Запоминаем первое число
        System.out.print("Введите второе число: ");
        scan = new Scanner(System.in);
        int secondNum = scan.nextInt(); // Запоминаем второе число

        if (firstNum > secondNum) // Сравниваем два числа
            System.out.println("Минимальное число: " + secondNum);
        else if (firstNum == secondNum) // Сравниваем равны ли числа
            System.out.println("Числа равны!");
        else System.out.println("Минимальное число: " + firstNum);
    }
}
