package ru.rsharipov.task3;

import java.util.Scanner;

public class Progression {

    public static void main(String[] args) {

        System.out.print("Введите первый член арифметической прогрессии: ");
        Scanner enter = new Scanner(System.in);
        int progrMember = enter.nextInt();
        System.out.print("Введите общее число членов прогрессии: ");
        enter = new Scanner(System.in);
        int totalMembers = enter.nextInt();
        System.out.print("Введите шаг прогрессии: ");
        enter = new Scanner(System.in);
        int step = enter.nextInt();

        for (int i = 1; i <= totalMembers; i++) {
            if (i != totalMembers )
                System.out.print(progrMember + ", "); // Вывод членов прогрессии
            else System.out.print(progrMember); // Вывод последнего члена прогрессии (без запятой)
            progrMember = progrMember + step; // Увеличение члена прогрессии на шаг прогрессии
        }
    }
}
