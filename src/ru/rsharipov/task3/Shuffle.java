package ru.rsharipov.task3;

import java.util.Scanner;

public class Shuffle {

    public static void main(String[] args) {

        System.out.println("Введите слово: ");
        Scanner sc = new Scanner(System.in);
        String word = sc.next();
        for (int i = word.length()-1; i >= 0; i--) {
            System.out.print(word.charAt(i)); // Вывод слова в обратном порядке
        }
    }
}
