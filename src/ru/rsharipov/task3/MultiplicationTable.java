package ru.rsharipov.task3;

public class MultiplicationTable {

    public static void main(String[] args) {

        for (int i = 1; i <= 10; i++) {
            for (int j = 1; j <= 10; j++) {
                System.out.print(i*j + " ");
                if (i * j < 10) // Для выравнивания таблицы проверяем число однозначное или двухзначное
                    System.out.print(" "); // К однозначному числу добавляем пробел
            }
            System.out.println(); // Разрыв строки
        }
    }
}
