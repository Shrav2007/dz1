package ru.rsharipov.task24;

import java.util.LinkedHashSet;
import java.util.Set;

public class App {
    public static void main(String[] args) {
        EvenLength el = new EvenLength(null);
        Set<String> set = new LinkedHashSet<>();
        set.add("foo");
        set.add("buzz");
        set.add("bar");
        set.add("fork");
        set.add("bort");
        set.add("spoon");
        set.add("!");
        set.add("dude");

        set = el.removeEvenLength(set); // Удалить все элементы четной длины из исходного множества set
        System.out.println(set);
    }
}
