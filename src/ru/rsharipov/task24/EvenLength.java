package ru.rsharipov.task24;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class EvenLength {
    Set<String> set = new LinkedHashSet<>();

    public EvenLength(Set<String> set) {
        this.set = set;
    }

    public Set<String> getSet() {
        return set;
    }

    public void setSet(Set<String> set) {
        this.set = set;
    }

    @Override
    public String toString() {
        return "EvenLength{" +
                "set=" + set +
                '}';
    }

    public Set<String> removeEvenLength(Set<String> set) {
        Iterator<String> it = set.iterator();
        while (it.hasNext()) {
            String entry = it.next();
            if ((entry.length()%2) == 0) { // Проверка элементов на четность длины
                it.remove(); // Удаление элементов четной длины из исходного множества
            }
        }
        return set;
    }
}
